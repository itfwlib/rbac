<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . '/modules/core/controllers/Controller.php';

class Menu extends Controller{
	public function __construct(){
		parent::__construct();
	}
	public function loadList(){
		$post = $this->input->post();
		$params = $post;
		$datatable = false;
		if(isset($post['draw'])){
			$datatable = true;
			$params['limit'] = $post['length'];
			$params['offset'] = $post['start'];
			$params['search'] = $post['search']['value'];
			$mode = 'LOADBY_PAGE';
		}else{
			$datatable = false;
			$mode = isset($post['mode']) ? $post['mode'] : 'LOAD_ALL';
		}
		
		// Loading Model
		$this->load->model('menu_model');
		$data = $this->menu_model->loadList($mode,$params);
		
		$rows = $data->data->result();
		if($datatable){
			$this->load->helper('generic');
			$output = toDatatable($rows,$data->total); 
		}else{
			$output = array(
				'code' => 200,
				'info' => 'Success',
				'data' => array(
					'rows'=> $rows,
					'total' => count($rows)));
		}
		echo json_encode($output);
		return true;
	}
	public function save(){
		$post = $this->input->post();
		$this->load->model('core/general_model');
		$result = $this->general_model->result();

		$this->load->library('form_validation');
		$this->form_validation->set_data($post);

		$this->form_validation->set_rules('id_menu', 'ID Menu', 'required');
		$this->form_validation->set_rules('label', 'Menu label', 'required');
		$this->form_validation->set_rules('references', 'References', 'required');
		if ($this->form_validation->run() == TRUE) {
			$this->load->model('menu_model');
			if($id = $this->menu_model->save($post)){
				$result->data = $id;
			}
		} else {
			$result->code = 501;
			$result->info = 'Failed to insert data';
		}
		echo json_encode($result);
	}
	public function delete(){
		$this->load->model('core/general_model');
		$result = $this->general_model->result();

		$delete_id = $this->input->post('menu_id');
		$this->load->model('menu_model');
		$delete = $this->menu_model->delete($delete_id);

		if($delete){
			$result->data = $delete;
		}else{
			$result->code = 501;
			$result->info = 'Failed deleting menu';
		}
		echo json_encode($result);
	}
}	