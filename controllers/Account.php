<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . '/modules/core/controllers/Controller.php';

class Account extends Controller{
	public function __construct(){
		parent::__construct();
	}
	public function loadList(){
		$post = $this->input->post();
		$params = array();
		$datatable = false;
		if(isset($post['draw'])){
			$datatable = true;
			$params['limit'] = $post['length'];
			$params['offset'] = $post['start'];
			$params['search'] = $post['search']['value'];
			$mode = 'LOADBY_PAGE';
		}else{
			$datatable = false;
			$mode = 'LOAD_ALL';
		}

		// Loading Model
		$this->load->model('account_model');
		$data = $this->account_model->loadList($mode,$params);
		$rows = $data->data->result();
		if($datatable){
			$this->load->helper('generic');
			$output = toDatatable($rows,$data->total); 
		}else{
			$output = array(
				'code' => 200,
				'info' => 'Success',
				'data' => array(
					'rows'=> $rows,
					'total' => $data->total));
		}
		echo json_encode($output);
		return true;
	}
	public function save(){
		$post = $this->input->post();
		$this->load->model('core/general_model');
		$result = $this->general_model->result();

		$this->load->library('form_validation');
		$this->form_validation->set_data($post);

		$this->form_validation->set_rules('id_account', 'ID Account', 'required');
		$this->form_validation->set_rules('username', 'Username', 'required|alpha_dash');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_emails');
		if($post['id_account'] == 0){
			$this->form_validation->set_rules('password', 'Password', 'required');
			$this->form_validation->set_rules('conf_password', 'Confirmation Password', 'required|matches[password]');
		}
		$this->form_validation->set_rules('account_status', 'Account Status', 'required');

		if ($this->form_validation->run() == TRUE) {
			$this->load->model('account_model');
			if($id = $this->account_model->save($post)){
				$result->data = $id;
			}
		} else {
			$result->code = 501;
			$result->info = validation_errors();;
		}
		echo json_encode($result);
	}
	public function delete(){
		$this->load->model('core/general_model');
		$result = $this->general_model->result();

		$delete_id = $this->input->post('role_id');
		$this->load->model('Account_model');
		$delete = $this->Account_model->delete($delete_id);

		if($delete){
			$result->data = $delete;
		}else{
			$result->code = 501;
			$result->info = 'Failed deleting Account';
		}
		echo json_encode($result);
	}
	public function load(){
		$post = $this->input->post();
		$this->load->model('account_model');

		$mode = isset($post['mode']) ? $post['mode'] : 'LOADBY_ID';

		$data = $this->account_model->load($mode,$post);
		echo json_encode($data);
		return true;
	}
}	