<?php
	class Role_model extends CI_MODEL{
		public function loadList($mode = 'LOAD_ALL',$params = array()){
			$totalData = $this->db->count_all_results('rbac_role');
			if($mode != 'LOADBY_PAGE'){
				switch ($mode) {
					case 'LOADBY_ID':
						$this->db->where('id_role',$params['id_role']);
						break;
				}
			}else{
				$this->db->limit($params['limit'],$params['offset']);
			}
			if(isset($params['search']) && !empty($params['search']) ){
				$this->db->where('lower(id_role) LIKE "%'.strtolower($params['search']).'%"');
				$this->db->or_where('lower(name) LIKE "%'.strtolower($params['search']).'%" ');
			}

			$q = $this->db->get('rbac_role');
			// echo $this->db->last_query();die();
			$data = new stdClass;
			$data->data = $q;
			$data->total = $totalData;
			return $data;
		}
		public function save($data = array()){
			$CI =& get_instance();
			$CI->load->model('core/general_model');
			
			$result = $this->general_model->result();
			if(!empty($data['id_role'])){
				// Update
				$id = $data['id_role'];
				unset($data['id_role']);
				$this->db->where('id_role',$id);
				$this->db->set($data);
				if($this->db->update('rbac_role')){
					return $id;
				}else{
					return false;
				}
			}else{
				if($this->db->insert('rbac_role',$data)){
					return $this->db->insert_id();
				}else{
					return false;
				}
			}
			return false;
		}
		public function delete($delete_id = null){
			if(!$delete_id){
				return false;
			}else{
				$this->db->where('id_role',$delete_id);
				$this->db->delete('rbac_role');
				return $delete_id;
			}
		}
	}